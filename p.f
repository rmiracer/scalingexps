        implicit none
        integer N
        parameter(N = 4096)
        integer init_interval, end_interval
        double precision L1, L2, x1, x2
        double precision temp1, temp2
        double precision buffer(6, N)
        double precision x_axis(N)
        double precision sum_x, sum_y, average_x, average_y
        double precision sum_error, error(N), average_error
        double precision sum_sigma_err, sigma_err
        double precision sum_numerator, sum_denominator
        double precision slope, n_fit
c        double precision i
        integer i
        integer arquivo, j

        open(10, file='output_structs/output_struct_tau_q1.dat',
     *       status='unknown')
        open(12, file='output_structs/output_struct_tau_q1.25.dat',
     *       status='unknown')
        open(15, file='output_structs/output_struct_tau_q1.5.dat',
     *       status='unknown')
        open(17, file='output_structs/output_struct_tau_q1.75.dat',
     *       status='unknown')
        open(20, file='output_structs/output_struct_tau_q2.dat',
     *       status='unknown')
        open(22, file='output_structs/output_struct_tau_q2.25.dat',
     *       status='unknown')
        open(25, file='output_structs/output_struct_tau_q2.5.dat',
     *       status='unknown')
        open(27, file='output_structs/output_struct_tau_q2.75.dat',
     *       status='unknown')
        open(30, file='output_structs/output_struct_tau_q3.dat',
     *       status='unknown')
        open(32, file='output_structs/output_struct_tau_q3.25.dat',
     *       status='unknown')
        open(35, file='output_structs/output_struct_tau_q3.5.dat',
     *       status='unknown')
        open(37, file='output_structs/output_struct_tau_q3.75.dat',
     *       status='unknown')
        open(40, file='output_structs/output_struct_tau_q4.dat',
     *       status='unknown')
        open(42, file='output_structs/output_struct_tau_q4.25.dat',
     *       status='unknown')
        open(45, file='output_structs/output_struct_tau_q4.5.dat',
     *       status='unknown')
        open(47, file='output_structs/output_struct_tau_q4.75.dat',
     *       status='unknown')
        open(50, file='output_structs/output_struct_tau_q5.dat',
     *       status='unknown')
        open(52, file='output_structs/output_struct_tau_q5.25.dat',
     *       status='unknown')
        open(55, file='output_structs/output_struct_tau_q5.5.dat',
     *       status='unknown')
        open(57, file='output_structs/output_struct_tau_q5.75.dat',
     *       status='unknown')
        open(60, file='output_structs/output_struct_tau_q6.dat',
     *       status='unknown')

        open(70, file='output_exps/normalized_zeta_q.dat',
     *       status='unknown')
        open(80, file='least_squares_interval.dat',
     *       status="old")
        open(90, file='output_exps/equation_linear_fitting.dat',
     *       status="unknown")

c       Load file
c        do i = 1, 6, 0.25
        i = 1
        do while (i.le.6)
          arquivo = int(i*10)
          do j = 1, N
            read(arquivo, *) x_axis(j), buffer(i, j)

c           Log scale
            x_axis(j) = log10(x_axis(j))
            buffer(i, j) = log10(buffer(i, j))
          enddo
c          i = i + 0.25
          i = i + 1
        enddo

        read(80, *) init_interval, end_interval

c        do i = 1, 6, 0.25
        i = 1
        do while (i.le.6)
          sum_x = 0.
          sum_y = 0.

          do j = init_interval, end_interval
            sum_x = sum_x + x_axis(j)
            sum_y = sum_y + buffer(i, j)
          enddo

          average_x = sum_x/(end_interval - init_interval)
          average_y = sum_y/(end_interval - init_interval)

          write(*, *) average_x, average_y

          sum_numerator = 0
          sum_denominator = 0

          do j = init_interval, end_interval
            sum_numerator = sum_numerator + (x_axis(j) -
     *        average_x)*(buffer(i, j) - average_y)
            sum_denominator = sum_denominator + (x_axis(j) -
     *        average_x)**2
          enddo

          slope = sum_numerator/sum_denominator
          n_fit = average_y - slope*average_x

          sum_error = 0.
          do j = init_interval, end_interval
            error(j) = (buffer(i, j) - (slope*x_axis(j) -
     *                       n_fit))**2
            sum_error = sum_error + error(j)
          enddo
          average_error = sum_error/(end_interval - init_interval)

          sum_sigma_err = 0.
          do j = init_interval, end_interval
            sum_sigma_err = sum_sigma_err + (error(j) -
     *                      average_error)**2
          enddo
          sigma_err = sqrt(sum_sigma_err/
     *                    (end_interval - init_interval - 2))

          write(70, *) i, slope, sigma_err
          write(90, *) 'y = ', slope, '*x + ', n_fit, ', Range: ',
     *                  x_axis(init_interval),
     *                  x_axis(end_interval)
c          i = i + 0.25
          i = i + 1
        enddo

c        do i = 1, 6, 0.25
        i = 1
        do while(i.le.6)
          close(int(i*10))
c          i = i + 0.25
          i = i + 1
        enddo

        end
